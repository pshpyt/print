Rails.application.routes.draw do
  
  root 'home#index'
  get '/:locale', to: 'home#index'


  scope '/(:locale)', locale: /en|fr|pt/ do    
    get 'a2c', to: "reports#index"
    resources :reports, path: '/a2c/reports' do
      member do
        get :competitors
      end

      collection do
      end
      
      collection do
        post :save_similar_properties   , defaults: { format: :json }
      end
    end

    # for A2C service related api 
    get 'a2c/:action',controller: :a2c
    # API
    get 'a2c/evolution_price', to: 'a2c#evolution_price', 
      defaults: { locale: :en, format: :json }
    get 'a2c/search_properties', to: 'a2c#search_properties', 
      defaults: { locale: :en, format: :json }

    
  end



end
