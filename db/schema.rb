# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160409182158) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "reports", force: :cascade do |t|
    t.integer  "property_type"
    t.integer  "rooms"
    t.string   "address"
    t.integer  "surface"
    t.integer  "price"
    t.decimal  "decimal",            precision: 8,  scale: 2
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.text     "description"
    t.string   "city"
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.decimal  "lat",                precision: 10, scale: 6
    t.decimal  "lng",                precision: 10, scale: 6
    t.string   "photo_path"
  end

  create_table "reports_similar_properties", force: :cascade do |t|
    t.integer "report_id"
    t.integer "similar_property_id"
  end

  add_index "reports_similar_properties", ["report_id"], name: "index_reports_similar_properties_on_report_id", using: :btree
  add_index "reports_similar_properties", ["similar_property_id"], name: "index_reports_similar_properties_on_similar_property_id", using: :btree

  create_table "similar_properties", force: :cascade do |t|
    t.text     "description"
    t.string   "title"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "photo_url"
    t.float    "price"
    t.float    "surface"
    t.integer  "observatory_id"
    t.string   "city"
  end

  add_foreign_key "reports_similar_properties", "reports"
end
