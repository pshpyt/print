class AddPhotoPathToReports < ActiveRecord::Migration
  def change
    add_column :reports, :photo_path, :string
  end
end
