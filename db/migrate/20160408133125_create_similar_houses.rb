class CreateSimilarHouses < ActiveRecord::Migration
  def change
    create_table :similar_houses do |t|
      t.integer :surface
      t.string :address
      t.text :description
      t.string :title
      t.string :image_url
      t.timestamps null: false
    end
  end
end
