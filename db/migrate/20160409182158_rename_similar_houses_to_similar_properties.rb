class RenameSimilarHousesToSimilarProperties < ActiveRecord::Migration
  def change
    rename_table :similar_houses, :similar_properties
    # Habtm
    rename_table :reports_similar_houses     , :reports_similar_properties
    remove_column :reports_similar_properties, :similar_house_id, :integer
    add_reference :reports_similar_properties, :similar_property, index: true
   # Change columns 
   remove_column :similar_properties, :address, :string
   remove_column :similar_properties, :image_url, :string

    add_column :similar_properties, :photo_url, :string
    add_column :similar_properties, :price    , :float
    remove_column :similar_properties, :surface, :integer
    add_column :similar_properties, :surface  , :float
    add_column :similar_properties, :observatory_id,  :integer
    add_column :similar_properties, :city ,  :string
  end
end
