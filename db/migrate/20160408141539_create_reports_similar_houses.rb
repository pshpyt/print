class CreateReportsSimilarHouses < ActiveRecord::Migration
  def change
    create_table :reports_similar_houses do |t|
      t.references :report, index: true, foreign_key: true
      t.references :similar_house, index: true, foreign_key: true
    end
  end
end
