class AddAttributesToProperties < ActiveRecord::Migration
  def change
    add_column :properties , :description , :text
    add_column :properties , :city        , :string
    add_column :properties , :name        , :string
    add_column :properties , :phone       , :string
    add_column :properties , :email       , :string
  end
end
