class AddMapsAttributesToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :lat, :decimal, { precision: 10, scale: 6 }
    add_column :properties, :lng, :decimal, { precision: 10, scale: 6 }
  end
end
