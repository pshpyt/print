class Report < ActiveRecord::Base
  paginates_per 20
  
  default_scope { order('created_at DESC') }
  
  has_and_belongs_to_many :similar_properties

  attr_accessor :image_json

  enum property_type: {house: 0,  flat: 1}

  before_validation :set_photo_base64
  after_save        :set_photo_path

  has_attached_file :photo, 
    styles: { 
      medium: "300x300>", 
      thumb: "100x100>" 
    }, 
    default_url: "/images/:style/missing.png"
    
  validates_attachment :photo, 
    content_type: 
    { 
      content_type: ["image/jpeg", "image/jpg", "image/png"] 
    }, size: { in: 0..10.megabytes }
  
  def set_photo_base64
    if image_json.present?
      image                   = Paperclip.io_adapters.for(image_json)
      extension               = image.content_type.split('/')[1]
      image.original_filename = "photo.#{extension}"
      self.photo              = image
    end
  end

  def set_photo_path
    if self.photo.present?
      self.photo_path = self.photo.url(:medium)
      self.update_column(:photo_path, self.photo.url(:medium))
    end
  end

end
