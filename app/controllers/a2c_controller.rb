class A2cController  < ApplicationController

  protect_from_forgery except: :evolution_price
  skip_before_action  :set_locale, only: [:evolution_price]

	# API 
	def stats
		data = {
			median_price: rand(1000),
			on_market_similar_props: rand(1000),
			sold_similar_props: rand(1000),			
		}
		render json:  data.to_json
	end

   def search_properties
      price   = params[:price].to_i
      surface = params[:surface].to_f
      city    = params[:city]

      # maybe futur param?
      price_margin = price * 0.10
      surface_margin = surface * 0.10

      # fake service
      observatory = ObservatoryService.new()    
      data = observatory.search(
      	price: price, 
        surface: surface, 
        city: city, 
        price_margin: price_margin, 
        surface_margin: surface_margin)

      render json: { properties: data }
  end

  def evolution_price 
    data = [{
      name: 'Tokyo',
      data: [10,9,8,7,6,5,4,3,2,1]
    }, {
      name: 'London',
      data: [1,2,3,4,5,6,7,8,9,10]
    }]
    render json: { data: data }
  end

end
