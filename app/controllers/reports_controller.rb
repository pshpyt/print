class ReportsController < ApplicationController
  before_action :set_report, only: [:show, :edit, :update, :destroy]

  def index
    @reports = has_search_params? ? filter_search : Report.all
    @reports = @reports.page(params[:page]) if @reports.any?
  end

  def show
    @report = @report.to_json(include: :similar_properties)
  end

  def new
    @report = Report.new
  end

  def edit
    @report = @report.to_json(include: :similar_properties)
  end

  def competitors
    @report = Report
      .find(params[:id])
      .to_json(include: :similar_properties)
      .html_safe 
  end


  def save_similar_properties
    properties = params[:properties]
    report = Report.find(params[:report_id])
    report.similar_properties.destroy_all
    if properties.present?
      properties.each do |property_id| 
        report.similar_properties << SimilarProperty.find(property_id)
      end
    end
    render json: { similars: report.similar_properties }, status: 200
  end

  def create
    base64 = params[:report].try(:[], :photo).try(:[], :src)
    @report = Report.new(report_params.merge(image_json: base64))
    respond_to do |format|
      if @report.save
        format.html { redirect_to competitors_report_path(@report) }
        format.json { render :show, status: :created, location: @report }
      else
        format.html { render :new }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    base64 = params[:report].try(:[], :photo).try(:[], :src)
    respond_to do |format|
      if @report.update(report_params.merge(image_json: base64))
        format.html { redirect_to @report, notice: 'Report was successfully updated.' }
        format.json { render json: { report: @report }, status: :ok }
      else
        format.html { render :edit }
        format.json { render json: @report.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @report.destroy
    respond_to do |format|
      format.html { redirect_to properties_url, notice: 'Report was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_report
    @report = Report.find(params[:id])
  end

  def report_params
    params.require(:report).permit(:property_type,
                                   :photo,
                                   :rooms,
                                   :address,
                                   :surface,
                                   :price,
                                   :city,
                                   :description,
                                   :name,
                                   :phone,
                                   :email,
                                   :lat,
                                   :lng)
  end

  def filter_search
    name       = params[:contact_name]
    date_query = build_date_query

    if date_query.present? && name.present?
      date_query
        .where("name ILIKE ?", "%#{name}%")
    elsif date_query.nil? && name.present?
      Report
        .where("name ILIKE ?", "%#{name}%")
    elsif name.blank? && date_query.present?
      date_query
    else
      Report.all
    end

  end

  def build_date_query
    to_date     = parse_date(params[:to_date])
    from_date   = parse_date(params[:from_date])
    valid_dates = valid_dates?(from_date, to_date)
    # If just initial date, get results starting from
    if from_date.present? && to_date.nil?
      Report
        .where('created_at >= ?', from_date.beginning_of_day)
    # If just last date, get results until date    
    elsif from_date.nil? && to_date.present?
      Report
        .where('created_at <= ?', to_date.end_of_day)
    elsif valid_dates
      Report
        .where(created_at: range(from_date, to_date))
    end    
  end

  def range(from, to)
    (from.beginning_of_day)..(to.end_of_day)
  end

  def parse_date(date)
    DateTime.parse(date)
  rescue
    nil
  end

  def valid_dates?(from, to)
    from.present? && to.present? && from <= to
  end

  def has_search_params?
    params[:contact_name].present? || params[:to_date].present? || params[:from_date].present?
  end
end
