app = angular.module("appController", ['ngMap']);

var controller = function($scope, $http){
  $scope.property = window.property;
  console.log($scope.property);
};

app.controller("showController", controller);

// Print if action print
$(document).ready(function(){
  if(/print=true/.test(location.href))
    window.setTimeout( function(){ window.print() }, 4000 );

// Not submit form on submit
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
});
