app = angular.module("app", ['ngMap']);

var controller = function($scope, $http){
  var token = document.querySelector('meta[name=csrf-token]').content;
  $scope.step             = 1;
  $scope.report           = window.report;
  $scope.property         = window.report;
  $scope.relateds         = [];
  $scope.properties           = {};
  $scope.selectedRelateds = {}

  checkSelectedProperties = function(){
    if(!$scope.report.similar_properties) return false;
    for(var index in $scope.report.similar_properties){
      property = $scope.report.similar_properties[index];
      $scope.properties[property.id] = true;
    }
  }
  checkSelectedProperties();

  $scope.back = function(){ $scope.step -= 1 };
  $scope.next = function(){ $scope.step += 1 };

  $scope.searchRelateds = function(searchParams){
    var params = $.param({ authenticity_token: token, search: searchParams })
    $http.get('/a2c/search_properties?'+params).then(function(response){
                $scope.relateds = response.data.properties;
                $scope.step = 2;
              }); 
  };

  var getIdSelectedProperties = function(){
    selectedProperties = [];
    for(var propertyId in $scope.properties){
      var checked = $scope.properties[propertyId];
      if(checked) selectedProperties.push(parseInt(propertyId)) 
    }
    return selectedProperties;
  };

  $scope.saveRelateds = function(){
    selectedProperties = getIdSelectedProperties() || [];
    if(selectedProperties.length > 6){
      alert('Allowed Max 6 related properties per report');
    } else {
      var params = { 
        authenticity_token: token, 
        report_id:          $scope.report.id, 
        properties:             selectedProperties 
      }
      $http.post('/a2c/reports/save_similar_properties', params).then(function(response){
        $scope.selectedRelateds = response.data.similars; 
      });
    }
  };

  var getUpdateSelectedRelateds = function(){
    var selectedIds = getIdSelectedProperties();
    var selectedRelateds = [];
    for(var i in selectedIds){
      var id = parseInt(selectedIds[i]);
      for(var o in $scope.relateds){
        if($scope.relateds[o].id == id){
          selectedRelateds.push($scope.relateds[o]);
        }
      }
    }
    $scope.selectedRelateds = selectedRelateds;
  };

  $scope.$watch('step', function(){
    if($scope.step == 3) {
      getUpdateSelectedRelateds();
      $scope.property.similar_properties = $scope.selectedRelateds;
      setTimeout(function(){
        google.maps.event.addDomListener(window, "resize", function() {
          var center = $scope.map.getCenter();
          $scope.map.setCenter(center); 
        });
        google.maps.event.trigger($scope.map, "resize");
      }, 2000);
    }
  });

};

app.controller("relatedController", controller);





